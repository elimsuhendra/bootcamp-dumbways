jQuery(function ($) {

  //#main-slider
  $(function () {
    $('#main-slider.carousel').carousel({
      interval: 8000
    });
  });


  //Initiat WOW JS
  new WOW().init();

  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });
  $('.scrollup').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 1000);
    return false;
  });

  // portfolio filter
  $(window).load(function () {
    'use strict';
    var $portfolio_selectors = $('.portfolio-filter >li>a');
    var $portfolio = $('.portfolio-items');
    $portfolio.isotope({
      itemSelector: '.portfolio-item',
      layoutMode: 'fitRows'
    });

    $portfolio_selectors.on('click', function () {
      $portfolio_selectors.removeClass('active');
      $(this).addClass('active');
      var selector = $(this).attr('data-filter');
      $portfolio.isotope({ filter: selector });
      return false;
    });
  });


  //Pretty Photo
  $("a[rel^='prettyPhoto']").prettyPhoto({
    social_tools: false
  });


  $(".add_item").click(function(){
      var i = $(".table_item tbody tr").length;
      console.log(i);
      var $clone = $(".table_item tbody tr").first().clone();

      $clone.attr("id","row_item-"+(i+1));
      $clone.find(".item").html("");
      $clone.find(".qty").html("");
      $clone.find(".harga_satuan").html("");
      $clone.find(".subtotal").html("");
      // $clone.find(".trash").attr("id","");
      $clone.find(".trash").attr("id","trash-"+(i+1));
      console.log($clone);

      $(".table_item tbody").append($clone);



      $(".trash").click(function(){
          let id = $(this).attr("id").split("-");
          // console.log(id);
          id = id[1];
          // console.log(id);
          $("#row_item-"+id).html("");

          //count total
          countTotal();
      });

      $(".qty").change(function(){
          let val = $(this).val(); 
          // console.log(val)

          //count total
          countTotal();
      });

      $(".harga_satuan").change(function(){
          let val = $(this).val(); 
          // console.log(val);

           //count total
          countTotal();
      });
  });


  $(".qty").change(function(){
      let val = $(this).val(); 
      // console.log(val)

      //count total
      countTotal();
  });

  $(".harga_satuan").change(function(){
      let val = $(this).val(); 
      // console.log(val);

       //count total
      countTotal();
  });

  $(".bayar_tunai").change(function(){
      let total = $(".total").val(); 
      console.log(total);
      let val = $(this).val();
      console.log(val);

      let kembali = val - total;
      // console.log(val);

      $(".kembali").val(kembali);
  });

  function countTotal(){
    
      var total = 0;

      $(".tbody_item tr").each(function(){
          console.log($(this));

          var qty = $(this).find(".qty").val();
          console.log(qty);
          var harga_satuan = $(this).find(".harga_satuan").val();
          console.log(harga_satuan);
          var subtotal = 0;

          subtotal = qty * harga_satuan;
          console.log(subtotal);
          $(this).find(".subtotal").val(subtotal);

          if(!isNaN(subtotal)){
               total += subtotal;          
          }else{

          }  
      });

      $(".total").val(total);
  }

  // $('#datetimepicker1').datetimepicker();
  // var datepicker = $.fn.datepicker.noConflict(); // return $.fn.datepicker to previously assigned value
  //   $.fn.bootstrapDP = datepicker;
  $(document).ready(function() {     
      $('.input-group.date').datepicker({
          format: 'yyyy/mm/dd'
      });
  });

  $(document).ready(function() {
    $('#js-date').datepicker();
});

  $(".btn_simpan").click(function(){
        let res = submit();

        alert(res);
  });

  function submit(){
      let nama = $(".nama_pelanggan").val();
      let tanggal = $(".tanggal").val();
      
      let arr_item = [];
      $(".tbody_item tr").each(function(){
          console.log($(this));
          var item_name = $(this).find(".item").val();
          var qty = $(this).find(".qty").val();
          console.log(qty);
          var harga_satuan = $(this).find(".harga_satuan").val();
          console.log(harga_satuan);
          var subtotal = $(this).find(".subtotal").val();

          let temp = {
            "Item":item_name,
            "Qty":qty,
            "HargaSatuan":harga_satuan,
            "SubTotal":subtotal
          };

          arr_item.push(temp);
      });

      let jam = $(".jam").val();
      let total = $(".total").val();
      let bayar_tunai = $(".bayar_tunai").val();
      let kembali = $(".kembali").val();

      var arr = {
        "NamaPelanggan":nama,
        "Tanggal":tanggal,
        "Jam":jam,
        "Total":total,
        "BayarTunai":bayar_tunai,
        "Kembali":kembali,
        "DetilPenjualan":arr_item
      };

      var res = JSON.stringify(arr);

      return res;
  }

});
