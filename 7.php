<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "dumbways";

  // Create connection
   $conn = new mysqli($servername, $username, $password, $dbname);

  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }
  echo "Connected successfully";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dumbways</title>

  <!-- Bootstrap -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/animate.css">
  <link href="assets/css/prettyPhoto.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet" />
  <!-- =======================================================
    Theme Name: Company
    Theme URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <header>
    
  </header>

  <div class="row index-wrap">
      <div class="status alert alert-success" style="display: none"></div>
      <div class="col-md-12">
          <div class="form-group">
            <!-- <label for="inputEmail3" class="col-sm-2 control-label">Item yang dijual</label> -->
            <div class="col-sm-10 item_wrapper"> 
                <table class="table table-bordered table_item">
                  <thead>
                    <tr> 
                        <th>Title</th> 
                        <th>Username</th> 
                        <th>Comment</th> 
                    </tr> 
                  </thead>
                  <tbody class="tbody_item"> 
                     <?php
                        $sql = "SELECT a.title, b.username, c.comment FROM posts a
                                JOIN users b ON b.id = a.createdBy
                                JOIN comments c ON c.postId = a.id
                                WHERE a.id = '1'
                                ORDER BY a.id DESC";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            // output data of each row
                            while($row = $result->fetch_assoc()) {
                                echo '<tr class="rows"> 
                                        <td>
                                            <input type="text" name="" class="form-control" id="" data-msg="" value="'.$row['title'].'"/>
                                        </td> 
                                        <td>
                                            <input type="text" name="" class="form-control" id="" data-msg="" value="'.$row['username'].'"/>
                                        </td> 
                                        <td>
                                            <input type="text" name="" class="form-control" id="" data-msg="" value="'.$row['comment'].'"/>
                                        </td> 
                                    </tr>';
                            }
                        } else {
                            echo "0 results";
                        }
                        $conn->close();
                    ?>
                    <!--   <tr class="rows"> 
                        <td>
                            <input type="text" name="" class="form-control" id="" data-msg="" value="test"/>
                        </td> 
                        <td>
                            <input type="text" name="" class="form-control" id="" data-msg="" value="test"/>
                        </td> 
                        <td>
                            <input type="text" name="" class="form-control" id="" data-msg="" value="test"/>
                        </td> 
                        <td class="td_trash" style="text-align: center;"> 
                            <a class="glyphicon glyphicon-trash trash" id="trash-1"></a>
                        </td> 
                    </tr>  -->
                  </tbody>
                </table>
            </div>
            <div class="validation"></div>
          </div>
        </div>
      </div>
    </div>

  <footer>
    <div class="footer">
      <div class="container">
       
       
      </div>

      <div class="pull-right">
        <!-- <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a> -->
      </div>
    </div>
  </footer>



  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="assets/js/jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>
