function hitungKembalian(total, jumlahUang){
    var result = '';
	var kelipatan = [500, 1000, 2000, 5000, 10000, 20000, 50000];
    kelipatan.reverse();
    // console.log(kelipatan);
    var kembalian = jumlahUang - total;
    // console.log(kembalian);
	for (var i = 0; i < kelipatan.length; i++) {
	    var modulus = kembalian % kelipatan[i];
	    console.log(modulus);
	    var jumlahKelipatan;
	    if(modulus == 0){
	        jumlahKelipatan = kembalian / kelipatan[i];
	        result += jumlahKelipatan+"x "+kelipatan[i]+"<br>";
	        
	        break;
	    }else{
	        jumlahKelipatan = (kembalian-modulus) / kelipatan[i];
	        kembalian = modulus; 
	        
	        if(jumlahKelipatan > 0){
	            result += jumlahKelipatan+"x "+kelipatan[i]+"<br>";
	        }
	    }
	}
	
// 	console.log(result);
	return result;
}

var total = 105000;
var jumlahUang = 200000;
var res = hitungKembalian(total, jumlahUang);
document.write(res);